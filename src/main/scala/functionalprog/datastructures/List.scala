package functionalprog.datastructures

sealed trait List[+A]
case object Nil extends List[Nothing]
case class Cons[+A](head: A, tail: List[A]) extends List[A]

object List {
  def sum(ints: List[Int]): Int = ints match {
    case Nil => 0
    case Cons(x, xs) => x + sum(xs)
  }

  def tail[A](l: List[A]) = l match {
    case Cons(x, xs) => xs
    case Nil => Nil
  }

  def setHead[A](h: A, l: List[A]) = l match {
    case Cons(x, xs) => Cons(h, xs)
    case Nil => Nil
  }

  def drop[A](l: List[A], n: Int): List[A] = n match {
    case x if x <= 0 => l
    case _ => drop(tail(l), n - 1)
  }

  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
    case Nil => Nil
    case Cons(x, xs) => if (f(x)) dropWhile(tail(l), f) else l
  }

  def init[A](l: List[A]): List[A] = l match {
    case Nil => Nil
    case Cons(x, xs) => if (xs == Nil) Cons(x, Nil) else init(xs)
  }

  def apply[A](as: A*): List[A] =
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))


  def length[A](as: List[A]): Int = foldRight(as, 0)((a: A, acc: Int) => acc + 1)

  @annotation.tailrec
  def foldLeft[A,B](as: List[A], z: B)(f: (B, A) => B): B = as match {
    case Nil => z
    case Cons(x, xs) => foldLeft(xs, f(z, x))(f)
  }

  def append[A](l: List[A], r: List[A]): List[A] = foldRight(l, r)(Cons(_,_))

  def foldRight[A,B](as: List[A], z: B)(f: (A, B) => B): B =
    as match {
      case Nil => z
      case Cons(x, xs) => f(x, foldRight(xs, z)(f))
    }

  def foldLeftViaFoldRight[A,B](l: List[A], z: B)(f: (B,A) => B): B =
    foldRight(l, (b:B) => b)((a,g) => b => g(f(b,a)))(z)

  def sum2(as: List[Int]) = foldLeft(as, 0)(_ + _)
  def product(as: List[Int]) = foldLeft(as, 1)(_ * _)

  def lengthFL[A](as: List[A]): Int = foldLeft(as, 0)((z, b) => z + 1)

  def reverse[A](as: List[A]): List[A] = foldLeft(as, List[A]())((z, b) => Cons(b, z))

  def concat[A](as: List[List[A]]): List[A] = {
    def loop(acc: List[A], as: List[List[A]]): List[A] = as match {
      case Nil => acc
      case Cons(l, ls) => loop(append(acc, l), ls)
    }
    loop(List[A](), as)
  }

  def transformAddOne(as: List[Int]): List[Int] = foldRight(as, List[Int]())((e: Int, acc: List[Int]) => Cons(e + 1, acc))
  def transformDoubleToString(as: List[Double]): List[String] = foldRight(as, List[String]())((e: Double, acc: List[String]) => Cons(e.toString, acc))

  def map[A,B](as: List[A])(f: A => B): List[B] = foldRight(as, List[B]())((a: A, b: List[B]) => Cons(f(a), b))
  def filter[A](as: List[A])(f: A => Boolean): List[A] = foldRight(as, List[A]())((a: A, b: List[A]) => if (f(a)) Cons(a, b) else b)
  def flatMap[A,B](as: List[A])(f: A => List[B]): List[B] = foldRight(as, List[B]())((a: A, b: List[B]) => append(f(a), b))
  def flatMapFilter[A](as: List[A])(f: A => Boolean): List[A] = {
    def g(a: A) =  if (f(a)) List(a) else List[A]()
    flatMap(as)(g)
  }
  def zipWith[A](a: List[A], b: List[A])(f: (A, A) => A): List[A] = {
    a match {
      case Nil => Nil
      case Cons(x, xs) => b match {
        case Nil => Nil
        case Cons(y, ys) => Cons(f(x, y), zipWith(xs,ys)(f))
      }
    }
  }

  def hasSubsequence[A](a: List[A], b: List[A]): Boolean = {
    def loop(a: List[A], subseq: List[A], searching: Boolean): Boolean = {
      if (subseq == Nil) true
      else a match {
        case Nil => false
        case Cons(x, xs) => subseq match {
          case Cons(y, ys) => if (x == y) loop(xs, ys, true)
          else loop(xs, b, false)
          case _ => false
        }
      }
    }
    loop(a, b, false)
  }
}
