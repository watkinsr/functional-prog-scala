package functionalprog.datastructures

sealed trait Tree[+A]
case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
object Tree {
  def size[A](t: Tree[A]): Int = t match {
    case Leaf(l) => 1
    case Branch(l, r) => 1 + size(l) + size (r)
  }
  def maximum(t: Tree[Int]): Int = t match {
    case Leaf(l) => l
    case Branch(l, r) => maximum(l) max maximum(r)
  }
  def depth(t: Tree[Int]): Int = t match {
    case Leaf(l) => 1
    case Branch(l, r) => (1 + depth(l)) max (1 + depth(r))
  }
  def map[A](t: Tree[A])(f: A => A): Tree[A] = t match {
    case Leaf(l) => Leaf(f(l))
    case Branch(l, r) => Branch(map(l)(f), map(r)(f))
  }
  def fold[A, B](t: Tree[A])(z: A => B)(f: (B, B) => B): B = t match {
    case Leaf(l) => z(l)
    case Branch(l, r) => f(fold(l)(z)(f), fold(r)(z)(f))
  }
  def maxFold(t: Tree[Int]): Int = Tree.fold(t)(t => t)(_ max _)
  def depthFold(t: Tree[Int]): Int = Tree.fold(t)(t => 1)(1 + _ max 1 + _)
  def sizeFold[A](t: Tree[A]): Int = Tree.fold(t)(t => 1)(1 + _ + _)
  def mapFold[A,B](t: Tree[A])(f: A => B): Tree[B] = fold(t)(l => Leaf(f(l)): Tree[B])(Branch(_,_))
}
