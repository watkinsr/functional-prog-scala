package functionalprog.errorhandling

import scala.{Either => _}
import functionalprog.datastructures._

sealed trait Either[+E, +A] {
  def map[B](f: A => B): Either[E, B] = this match {
    case Right(a) => Right(f(a))
    case Left(e) => Left(e)
  }
  def flatMap[EE >: E, B](f: A => Either[EE, B]): Either[EE, B] = this match {
    case Right(a) => f(a)
    case Left(e) => Left(e)
  }
  def orElse[EE >: E,B >: A](b: => Either[EE, B]): Either[EE, B] = this match {
    case Right(a) => Right(a)
    case Left(e) => b
  }
  def map2[EE >: E, B, C](b: Either[EE, B])(f: (A, B) => C): Either[EE, C] =
    this flatMap(aa => b map(bb => f(aa, bb)))

  def map2TwoE[EE >: E, B, C](b: Either[EE, B])(f: (A, B) => C)(g: (EE, EE) => EE): Either[EE, C] = {
    this match {
      case Right(a) => b match {
        case Right(b) => Right(f(a, b))
        case Left(e) => Left(e)
      }
      case Left(e) => b match {
        case Right(b) => Left(e)
        case Left(e2) => Left(g(e, e2))
      }
    }
  }

}
case class Left[+E](value: E) extends Either[E, Nothing]
case class Right[+A](value: A) extends Either[Nothing, A]

object Either {
  def traverse[E, A, B](as: List[A])(f: A => Either[E, B]): Either[E, List[B]] = {
    val z = Right(List[B]()): Either[E, List[B]]
    List.foldRight(as, z)((a, b) => b flatMap(bb => f(a) map (aa => Cons(aa, bb))))
  }
  def sequence[E, A](es: List[Either[E, A]]): Either[E, List[A]] = {
    traverse(es)(_ map(identity))
  }
  def Try[A](a: => A): Either[Exception, A] =
    try Right(a)
    catch { case e: Exception => Left(e) }
}

case class Person(name: Name, age: Age)
sealed class Name(val value: String)
sealed class Age(val value: Int)
object Person {
  def mkName(name: String): Either[String, Name] =
    if (name == "" || name == null) Left("Name is empty.")
    else Right(new Name(name))
  def mkAge(age: Int): Either[String, Age] =
    if (age < 0) Left("Age is out of range.")
    else Right(new Age(age))
  def mkPerson(name: String, age: Int): Either[String, Person] =
    mkName(name).map2TwoE(mkAge(age))(Person(_, _))(_ + " " + _)
}
