package functionalprog.testing

import functionalprog.state._
import Gen._
import Prop._
import java.util.concurrent.Executors
import functionalprog.parallel._
import functionalprog.parallel.Par.Par
import language.postfixOps
import language.implicitConversions
import java.util.concurrent.ExecutorService

case class Prop(run: (MaxSize,TestCases, RNG) => Result) {
  def &&(p: Prop): Prop = Prop((s, tc, r) => (run(s,tc, r)) match {
    case (Falsified(a,b)) => Falsified(a,b)
    case (_) => p.run(s,tc,r)
  })
  def ||(p: Prop): Prop = Prop((s,tc, r) => run(s,tc, r) match {
    case (Passed) => Passed
    case _ => p.run(s,tc, r)
  })
}

object Prop {
  type TestCases = Int
  // type Result = Either[(FailedCase, SuccessCount), SuccessCount]
  type FailedCase = String
  type SuccessCount = Int

  type MaxSize = Int

  sealed trait Result {
    def isFalsified: Boolean
    // Indicates that all tests passed
  }
  case object Passed extends Result {
    def isFalsified = false
    // Indicates that one of the test
  }
  // cases falsified the property
  case class Falsified(failure: FailedCase, successes: SuccessCount) extends Result {
    def isFalsified = true
  }
  case object Proved extends Result {
    def isFalsified = false
  }
  // def check: Either[(FailedCase, SuccessCount), SuccessCount]

  def run(p: Prop,
    maxSize: Int = 100,
    testCases: Int = 100,
    rng: RNG = RNG.Simple(System.currentTimeMillis)): Unit =
    p.run(maxSize, testCases, rng) match {
      case Falsified(msg, n) =>
        println(s"! Falsified after $n passed tests:\n $msg")
      case Passed =>
        println(s"+ OK, passed $testCases tests.")
      case Proved =>
        println(s"+ OK, proved property.")
    }
  def check(p: => Boolean): Prop = Prop { (_, _, _) =>
    if (p) Proved else Falsified("()", 0)
  }
}


case class Gen[A](sample: State[RNG,A]){
  def map[B](f: A => B): Gen[B] =
    Gen(sample.map(f))

  def map2[B,C](g: Gen[B])(f: (A,B) => C): Gen[C] =
    Gen(sample.map2(g.sample)(f))

  def flatMap[B](f: A => Gen[B]): Gen[B] =
    Gen(sample.flatMap(a => f(a).sample))

    /* A method alias for the function we wrote earlier. */
  def listOfN(size: Int): Gen[List[A]] =
    Gen.listOfN(size, this)

  def listOfN(size: Gen[Int]): Gen[List[A]] = size.flatMap(a => Gen.listOfN(a, this))
  def unsized: SGen[A] = SGen(_ => this)
  def **[B](g: Gen[B]): Gen[(A,B)] =
    (this map2 g)((_,_))
}

object Gen {
  def unit[A](a: => A): Gen[A] =
    Gen(State.unit(a))

  val boolean: Gen[Boolean] =
    Gen(State(RNG.boolean))

  def choose(start: Int, stopExclusive: Int): Gen[Int] =
    Gen(State(RNG.nonNegativeInt).map(n => start + n % (stopExclusive-start)))

  def listOfN[A](n: Int, g: Gen[A]): Gen[List[A]] =
    Gen(State.sequence(List.fill(n)(g.sample)))

  def union[A](g1: Gen[A], g2: Gen[A]): Gen[A] = {
    choose(0, 2).flatMap(a => if (a % 2 == 0) g1 else g2)
  }

  def weighted[A](g1: (Gen[A],Double), g2: (Gen[A],Double)): Gen[A] = {
    val (w1, w2) = (g1._2 * 100, g2._2 * 100)
    val (gen1, gen2) = (g1._1, g2._1)
    choose(0, 101).flatMap(a => if (a <= w1) gen1 else gen2)
  }

  def weighted_2[A](g1: (Gen[A],Double), g2: (Gen[A],Double)): Gen[A] = {
    /* The probability we should pull from `g1`. */
      val g1Threshold = g1._2.abs / (g1._2.abs + g2._2.abs)

      Gen(State(RNG.double).flatMap(d => if (d < g1Threshold) g1._1.sample else g2._1.sample))
    }

  def randomStream[A](g: Gen[A])(rng: RNG): Stream[A] =
    Stream.unfold(rng)(rng => Some(g.sample.run(rng)))

  def buildMsg[A](s: A, e: Exception): String =
    s"test case: $s\n" +
  s"generated an exception: ${e.getMessage}\n" +
  s"stack trace:\n ${e.getStackTrace.mkString("\n")}"


  def forAll[A](as: Gen[A])(f: A => Boolean): Prop = Prop {
    (_, n,rng) => randomStream(as)(rng).zip(Stream.from(0)).take(n).map {
      case (a, i) => try {
        if (f(a)) Passed else Falsified(a.toString, i)
      } catch { case e: Exception => Falsified(buildMsg(a, e), i) }
    }.find(_.isFalsified).getOrElse(Passed)
  }

  def forAll[A](g: SGen[A])(f: A => Boolean): Prop =
    forAll(g(_))(f)

  def forAll[A](g: Int => Gen[A])(f: A => Boolean): Prop = Prop {
    (max,n,rng) =>
    val casesPerSize = (n + (max - 1)) / max
    val props: Stream[Prop] =
      Stream.from(0).take((n min max) + 1).map(i => forAll(g(i))(f))
    val prop: Prop =
      props.map(p => Prop { (max, _, rng) =>
        p.run(max, casesPerSize, rng)
      }).toList.reduce(_ && _)
      prop.run(max,n,rng)
  }
  def listOf[A](g: Gen[A]): SGen[List[A]] =
    SGen(h => Gen.listOfN(h, g))

  def listOf1[A](g: Gen[A]): SGen[List[A]] =
    SGen(h => g.listOfN(h max 1))

  val smallInt = Gen.choose(-10,10)
  val maxProp = Gen.forAll(Gen.listOf(smallInt)) { ns =>
    ns match {
      case x :: xs => {
        val max = ns.max
        !ns.exists(_ > max)
      }
      case Nil => true
    }
  }
  val sortedProp = Gen.forAll(Gen.listOf(smallInt)) { ns =>
    val sorted = ns.sorted
    if (ns.isEmpty) true
    else {
      val max = ns.max
      val min = ns.min
        (max == sorted.last) && (min == sorted.head)
    }
  }

  val S = weighted(
    Gen.choose(1,4).map(Executors.newFixedThreadPool) -> .75,
    unit(Executors.newCachedThreadPool) -> .25
  )

  object ** {
    def unapply[A,B](p: (A,B)) = Some(p)
  }

  def forAllPar[A](g: Gen[A])(f: A => Par[Boolean]): Prop =
    forAll(S ** g) { case s ** a => f(a)(s).get }

  def checkPar(p: Par[Boolean]): Prop =
    forAllPar(Gen.unit(()))(_ => p)

  def equal[A](p: Par[A], p2: Par[A]): Par[Boolean] =
    Par.map2(p,p2)(_ == _)

  val ES: ExecutorService = Executors.newCachedThreadPool

  val p3 = check {
    equal (
      Par.map(Par.unit(1))(_ + 1),
      Par.unit(2)
    ) (ES) get
  }

}

case class SGen[A](s: Int => Gen[A]) {
  def apply(n: Int): Gen[A] = s(n)

  def flatMap[B](f: A => Gen[B]): SGen[B] =
    SGen(h => s(h).flatMap(f))

}
