package functionalprog

import functionalprog.testing._

object Main extends App {
  Prop.run(Gen.maxProp)
  Prop.run(Gen.sortedProp)
  Prop.run(Gen.p3)
}
