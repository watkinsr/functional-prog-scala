package functionalprog.laziness

sealed trait Stream[+A] {
  def toList: List[A] = this match {
    case Empty => Nil: List[A]
    case Cons(h, t) => h() :: t().toList
  }
  def take(n: Int): Stream[A] = {
    if (n <= 0) Empty
    else this match {
      case Empty => Empty
      case Cons(h, t) => Cons(h, () => t().take(n -1))
    }
  }
  def takeWhile(p: A => Boolean): Stream[A] = {
    this match {
      case Empty => Empty
      case Cons(h, t) => if (p(h())) Cons(h, () => t().takeWhile(p)) else Empty
    }
  }

  def drop(n: Int): Stream[A] = {
    if (n <= 0) this
    else this match {
      case Empty => Empty
      case Cons(h, t) => t().drop(n - 1)
    }
  }

  def foldRight[B](z: => B)(f: (A, => B) => B): B =
    this match {
      case Cons(h,t) => f(h(), t().foldRight(z)(f))
      case _ => z
    }

  def exists(p: A => Boolean): Boolean =
    foldRight(false)((a, b) => p(a) || b)

  def forAll(p: A => Boolean): Boolean =
    foldRight(true)((a, b) => p(a) && b)

  def takeWhileFR(p: A => Boolean) = {
    foldRight(Empty: Stream[A])((a, b) => if (p(a)) Stream.cons(a, b) else Empty)
  }

  def headOption: Option[A] = {
    foldRight(None: Option[A])((a, b) => if (a != Empty) Some(a) else None)
  }

  def map[B](f: A => B): Stream[B] = {
    foldRight(Empty: Stream[B])((a, b) => Stream.cons(f(a), b))
  }

  def filter(f: A => Boolean): Stream[A] =
    foldRight(Empty: Stream[A])((a, b) => if (f(a)) Stream.cons(a, b) else b)

  def append[B>:A](s: => Stream[B]): Stream[B] =
    foldRight(s)((h,t) => Stream.cons(h,t))

  def flatMap[B>:A](f: A => Stream[B]): Stream[B] =
    foldRight(Stream.empty: Stream[B])((a, b) => f(a) append b)

}

case object Empty extends Stream[Nothing]
case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]
object Stream {
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }
  def empty[A]: Stream[A] = Empty
  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty else cons(as.head, apply(as.tail: _*))
  def constant[A](a: A): Stream[A] = cons(a, constant(a))

  def from(n: Int): Stream[Int] = cons(n, from(n + 1))

  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ => fib(n - 1) + fib(n -2)
  }
  def fibs: Stream[Int] = from(0) map fib

  val fibs2 = {
    def go(f0: Int, f1: Int): Stream[Int] =
      cons(f0, go(f1, f0+f1))
    go(0, 1)
  }

  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = f(z) match {
    case Some((a, s)) => cons(a, unfold(s)(f))
    case None => empty
  }

  def fibsUnfold: Stream[Int] = unfold((0, 1))(a => {
    val f = a._1 + a._2
    Some(a._1, (a._2, a._1 + a._2))
  })

  def fromUnfold(n: Int): Stream[Int] = unfold(n)(a => Some(a, a + 1))

  def constantUnfold[A](a: A): Stream[A] = unfold(a)(a => Some(a, a))
  def ones = constantUnfold(1)

  def unfoldMap[A, B](as: Stream[A])(f: A => B): Stream[B] = unfold(as)(a => a match {
    case Cons(h, t) => Some((f(h()), t()))
    case Empty => None
  })

  def unfoldTake[A](as: Stream[A])(n: Int): Stream[A] = unfold((as, n))(a => {
    if (a._2 <= 0) None
    else {
      a._1 match {
        case Cons(h, t) => Some((h(), (t(), a._2 - 1)))
        case Empty => None
      }
    }
  })
  def unfoldTakeWhile[A](as: Stream[A])(p: A => Boolean): Stream[A] = unfold((as, p))(a => a._1 match {
    case Cons(h, t) => if (p(h())) Some((h()), (t(), p)) else None
    case Empty => None
  })
  def unfoldZipWith[A](as: Stream[A], bs: Stream[A])(f: (A, A) => A): Stream[A] = unfold((as, bs))(t => t._1 match {
    case Cons(x, xs) => t._2 match {
      case Cons(y, ys) => Some((f(x(), y()), (xs(), ys())))
      case Empty => None
    }
    case Empty => None
  })

  def zipAll[A, B](as: Stream[A], bs: Stream[B]): Stream[(Option[A], Option[B])] = unfold((as, bs)) {
    case (Empty, Cons(h, t)) => Some((None,Some(h())), (Empty, t()))
    case (Cons(h, t), Empty) => Some((Some(h()),None), (t(), Empty))
    case (Cons(x, xs), Cons(y, ys)) => Some((Some(x()), Some(y())), (xs(), ys()))
    case (Empty, Empty) => None
  }

  def startsWith[A](s: Stream[A], s1: Stream[A]): Boolean = {
    zipAll(s, s1).takeWhile (_._2.isEmpty) forAll {
      case (Some(x), Some(y)) => x == y
      case _ => false
    }
  }

  def tails[A](s: Stream[A]): Stream[Stream[A]] = unfold(s) {
    case Empty => None
    case s => Some((s, s drop 1))
  } append Stream(empty)

  def scanRight[A](z: A)(as: Stream[A])(f: (A, A) => A): Stream[A] = {
    as.foldRight(empty[A])((a, b) => cons(f(a, b match {
      case Cons(x, xs) => x()
      case Empty => z
    }), b)) append Stream(z)
  }
}
