package functionalprog.parallel

import java.util.concurrent._
import language.implicitConversions

object Examples {
  import Par._
  def foldLeft[A,B,C](z: A)(seq: IndexedSeq[A])(f: (A, A) => A): Par[A] =
    if (seq.size == 1) {
      unit(seq.headOption getOrElse z)
    }
    else {
      val (l,r) = seq.splitAt(seq.length/2)
      val pl = foldLeft(z)(l)(f)
      val pr = foldLeft(z)(r)(f)
      map2(pl, pr)(f)
    }

  /**
    def sum(ints: IndexedSeq[Int]): Int =
    if (ints.size <= 1)
    ints headOption getOrElse 0
    else {
    val (l,r) = ints.splitAt(ints.length/2)
    val sumL: Par[Int] = Par.unit(sum(l))
    val sumR: Par[Int] = Par.unit(sum(r))
    Par.get(sumL) + Par.get(sumR)
    }
    */

  def sum(ints: IndexedSeq[Int]): Par[Int] = foldLeft(0)(ints)(_ + _)
  def paragraphWordCount(paragraphs: IndexedSeq[String]): Par[Int] = {
    val pa = foldLeft("")(paragraphs)((s: String, acc: String) => s ++ " " + acc)
    map(pa)(_.split(" ").length)
  }
}

object Par {
  type Par[A] = ExecutorService => Future[A]
  def unit[A](a: A): Par[A] = es => UnitFuture(a)

  private case class UnitFuture[A](get: A) extends Future[A] {
    def isDone = true
    def get(timeout: Long, units: TimeUnit) = get
    def isCancelled = false
    def cancel(evenIfRunning: Boolean): Boolean = false
  }
  def map2[A,B,C](a: Par[A], b: Par[B])(f: (A, B) => C): Par[C] = (es: ExecutorService) => {
    val af = a(es)
    val bf = b(es)
    UnitFuture(f(af.get, bf.get))
  }

  def map3[A,B,C,D](a: Par[A], b: Par[B], c: Par[C])(f: (A, B, C) => D): Par[D] = {
    val g = (a: A, b: B) => c => f(a, b, c)
    val h = (a: C => D, b: C) => a(b)
    map2(map2(a,b)(g), c)(h)
  }
  def fork[A](a: => Par[A]): Par[A] = es => es.submit(new Callable[A] {
    def call = a(es).get
  })
  def lazyUnit[A](a: => A): Par[A] = fork(unit(a))
  def run[A](s: ExecutorService)(a: Par[A]): Future[A] = a(s)

  def asyncF[A,B](f: A => B): A => Par[B] = a => lazyUnit(f(a))

  def map[A,B](pa: Par[A])(f: A => B): Par[B] = map2(pa, unit(()))((a, _) => f(a))

  def sortPar(parList: Par[List[Int]]) = map(parList)(_.sorted)

  def sequence[A](ps: List[Par[A]]): Par[List[A]] =
    ps.foldRight(unit(List[A]()))((h,t) => map2(h,t)(_ :: _))


  def parMap[A,B](ps: List[A])(f: A => B): Par[List[B]] = fork {
    val fbs: List[Par[B]] = ps.map(asyncF(f))
    sequence(fbs)
  }

  def parFilter[A](as: List[A])(f: A => Boolean): Par[List[A]] = {
    val pars = as map (asyncF(a => if (f(a)) List(a) else List()))
    val t = map(sequence(pars))(_.flatten)
    t
  }

  def equal[A](es: ExecutorService)(p: Par[A], p2: Par[A]): Boolean =
    p(es).get == p2(es).get
}
