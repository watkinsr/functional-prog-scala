package functionalprog.datastructures

import org.scalatest._
import functionalprog.datastructures._

class ListSpec extends FlatSpec with Matchers {
  "The sum function" should "summate" in {
    List.sum(List(3,4,5,6,2)) shouldEqual 20
  }
  "The product function" should "compute the product of a list" in {
    List.product(List(3,4,5,6,2)) shouldEqual 720
  }
  "The length (foldLeft) function" should "compute the length of a list" in {
    List.length(List(3,4,5,6,2)) shouldEqual 5
  }
  "The reverse function" should "reverse the list" in {
    List.reverse(List(3,4,5,6,2)) shouldEqual List(2,6,5,4,3)
  }
  "The foldLeftViaFoldRight function" should "work" in {
    List.foldLeftViaFoldRight(List(3,4,5,6,2), 0)(_ + _) shouldEqual 20
  }
  "append" should "add to the front of List[A]" in {
    List.append(List(1), List(2,3)) shouldEqual List(1,2,3)
  }
  "concat" should "concat list of lists into single list" in {
    List.concat(List(List(1), List(2,3))) shouldEqual List(1,2,3)
    List.concat(List(List(1), List(2,3), List(4,5))) shouldEqual List(1,2,3,4,5)
  }
  "transformAddOne" should "add one to every element in list" in {
    List.transformAddOne(List(1,2,3)) shouldEqual List(2,3,4)
  }
  "transform each double to string in a list" should "contain the list with just strings" in {
    List.transformDoubleToString(List(1.0, 2.0, 3.0)) shouldEqual List("1.0", "2.0", "3.0")
  }
  "map" should "modify each element given an F" in {
    List.map(List(1.0, 2.0, 3.0))(_.toString) shouldEqual List("1.0", "2.0", "3.0")
    List.map(List(1,2,3))(_ + 1) shouldEqual List(2,3,4)
  }
  "filter" should "filter out elements given a predicate" in {
    List.filter(List(1,2,3))(_ % 2 == 0) shouldEqual List(2)
  }
  "flatMap" should "insert a resulting list into a list" in {
    List.flatMap(List(1,2,3))(i => List(i, i)) shouldEqual List(1,1,2,2,3,3)
  }
  "flatMapFilter" should "filter elements given a predicate" in {
    List.flatMapFilter(List(1,2,3))(_ % 2 == 0) shouldEqual List(2)
  }
  "zipWith" should "construct a list by applying f to corresponding elements" in {
    List.zipWith(List(1,2,3), List(4,5,6))(_ + _) shouldEqual List(5,7,9)
    List.zipWith(List(1,2,3), List(4,5))(_ + _) shouldEqual List(5,7)
    List.zipWith(List(1,2,3), List())(_ + _) shouldEqual List()
  }
  "hasSubsequence" should "detect a subsequence within a list" in {
    List.hasSubsequence(List(1,2,3), List(2)) shouldEqual true
    List.hasSubsequence(List(1,2,3), List(4)) shouldEqual false
    List.hasSubsequence(List(1,2,3), List(2,3)) shouldEqual true
    List.hasSubsequence(List(1,2,3), List(1,2)) shouldEqual true
    List.hasSubsequence(List(1,2,3), List(1,2,3)) shouldEqual true
  }
}
