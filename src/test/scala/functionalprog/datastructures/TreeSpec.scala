package functionalprog.datastructures

import org.scalatest._
import functionalprog.datastructures._

class TreeSpec extends FlatSpec with Matchers {
  "size" should "count a given Tree T's size" in {
    val t = Branch(Branch(Leaf(2), Leaf(3)), Branch(Leaf(4), Leaf(5)))
    Tree.size(t) shouldEqual 7
  }
  "max" should "get maximal element in a Tree" in {
    val t = Branch(Branch(Leaf(2), Leaf(3)), Branch(Leaf(4), Leaf(5)))
    Tree.maximum(t) shouldEqual 5
  }
  "depth" should "get max path length in a Tree" in {
    val t = Branch(Branch(Leaf(2), Leaf(3)), Branch(Leaf(4), Leaf(5)))
    val t2 = Branch(Branch(Leaf(2), Branch(Leaf(3), Leaf(3))), Branch(Leaf(4), Leaf(5)))
    Tree.depth(t) shouldEqual 3
    Tree.depth(t2) shouldEqual 4
  }
  "map" should "transform every leaf by f" in {
    val t = Branch(Branch(Leaf(2), Leaf(3)), Branch(Leaf(4), Leaf(5)))
    Tree.map(t)(_ + 1) shouldEqual Branch(Branch(Leaf(3), Leaf(4)), Branch(Leaf(5), Leaf(6)))
  }
  "maxFold" should "implement maximum" in {
    val t = Branch(Branch(Leaf(2), Leaf(3)), Branch(Leaf(4), Leaf(5)))
    Tree.maxFold(t) shouldEqual 5
  }
  "depthFold" should "implement depth" in {
    val t = Branch(Branch(Leaf(2), Leaf(3)), Branch(Leaf(4), Leaf(5)))
    Tree.depthFold(t) shouldEqual 3
  }
  "sizeFold" should "implement size" in {
    val t = Branch(Branch(Leaf(2), Leaf(3)), Branch(Leaf(4), Leaf(5)))
    Tree.sizeFold(t) shouldEqual 7
  }
  "mapFold" should "implement map" in {
    val t = Branch(Branch(Leaf(2), Leaf(3)), Branch(Leaf(4), Leaf(5)))
    Tree.mapFold(t)(_ + 1) shouldEqual Branch(Branch(Leaf(3), Leaf(4)), Branch(Leaf(5), Leaf(6)))
  }
}
