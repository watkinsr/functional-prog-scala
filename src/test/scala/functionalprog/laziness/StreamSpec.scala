package functionalprog.laziness

import org.scalatest._
import functionalprog.laziness._

class StreamSpec extends FlatSpec with Matchers {
  "The toList function" should "convert a Stream[A] to List[A]" in {
    Stream.cons(1, Stream.cons(2, Stream.empty)).toList shouldEqual List(1,2)
  }
  "The drop function" should "return the Stream without the first n elements" in {
    Stream(1,2,3,4,5,6).drop(3).toList shouldEqual Stream(4,5,6).toList
  }
  "The take function" should "return the Stream with the first n elements" in {
    Stream(1,2,3,4,5,6).take(3).toList shouldEqual Stream(1,2,3).toList
  }
  "The takeWhile function" should "return the Stream with the first n elements that match p" in {
    Stream(1,2,3,4,5,6).takeWhile(_ <= 3).toList shouldEqual Stream(1,2,3).toList
  }
  "The exists function" should "return true if some p is true for any x" in {
    Stream(1,2,3,4,5,6).exists(_ == 3) shouldEqual true
  }
  "The forAll function" should "return true if some p is true for all x in X" in {
    Stream(1,2,3,4,5,6).forAll(_ >= 0) shouldEqual true
  }
  "The takeWhileFR function" should "take largest prefix of elements that hold for p" in {
    Stream(1,2,3,4,5,6).takeWhileFR(_ <= 3).toList shouldEqual List(1,2,3)
    Stream(1,2,3,4,5,6).takeWhileFR(_ >= 7).toList shouldEqual Nil
    Stream(1,2,3,4,5,6).takeWhileFR(_ >= 2).toList shouldEqual Nil
  }
  "The headOption function" should "return the Some(head) if the Stream has any elements" in {
    Stream(1,2,3,4,5,6).headOption shouldEqual Some(1)
    val t = Stream.empty.headOption
    t shouldEqual None
  }
  "The map function" should "return the Stream with f applied to each element" in {
    Stream(1,2,3,4,5,6).map(a => a * a).toList shouldEqual List(1,4,9,16,25,36)
  }
  "The filter function" should "return the Stream with only elements that observe p" in {
    Stream(1,2,3,4,5,6).filter(a => a % 2 == 0).toList shouldEqual List(2,4,6)
    Stream(1,2,3,4,5,6).filter(a => a > 6).toList shouldEqual Nil
  }
  "The flatMap function" should "return the Stream[B] given an A => Stream[B]" in {
    Stream(1,2,3).flatMap(a => Stream(a, 2)).toList shouldEqual List(1,2,2,2,3,2)
  }
  "The constant function" should "return an infinite list given a value x" in {
    Stream.constant(1).take(5).toList shouldEqual List(1,1,1,1,1)
  }
  "The from function" should "return an infinite list from x to x + 1 ... n" in {
    Stream.from(2).take(5).toList shouldEqual List(2,3,4,5,6)
  }
  "The fibs function" should "generate an infinite fib Stream" in {
    Stream.fibs.take(5).toList shouldEqual List(0,1,1,2,3)
  }
  "The unfold function" should "build a stream given an f" in {
    Stream.unfold(0)(a => Some(a + 1, a + 1)).take(5).toList shouldEqual List(1,2,3,4,5)
  }
  "The fibsUnfold function" should "build an infinite fib sequence" in {
    Stream.fibsUnfold.take(5).toList shouldEqual List(0,1,1,2,3)
  }
  "The fromUnfold function" should "build an infinite sequence from x to x + 1 .. n" in {
    Stream.fromUnfold(2).take(5).toList shouldEqual List(2,3,4,5,6)
  }
  "constantUnfold" should "build an infinite sequence of x" in {
    Stream.constantUnfold(1).take(5).toList shouldEqual List(1,1,1,1,1)
  }
  "ones" should "build an infinite sequence of ones" in {
    Stream.ones.take(5).toList shouldEqual List(1,1,1,1,1)
  }
  "unfoldMap" should "build a stream by applying f to each x in X" in {
    Stream.unfoldMap(Stream(1,2,3))((a: Int) => a * a).take(5).toList shouldEqual List(1,4,9)
  }
  "unfoldTake" should "take n x in X" in {
    Stream.unfoldTake(Stream(1,2,3))(2).toList shouldEqual List(1,2)
    Stream.unfoldTake(Stream(1,2,3))(4).toList shouldEqual List(1,2,3)
    Stream.unfoldTake(Stream(1,2,3))(0).toList shouldEqual Nil
  }
  "unfoldTakeWhile" should "take largest prefix x in X for p" in {
    Stream.unfoldTakeWhile(Stream(1,2,3))(a => a <= 2).toList shouldEqual List(1,2)
  }
  "unfoldZipWith" should "zip together two Streams given an f" in {
    Stream.unfoldZipWith(Stream(1,2,3), Stream(4,5,6))((a, b) => a + b).toList shouldEqual List(5,7,9)
  }
  "startsWith" should "return true if s1 starts with s2" in {
    Stream.startsWith(Stream(1,2,3), Stream(1,2)) shouldEqual true
  }
  "tails" should "return all Stream suffixes of an input sequence" in {
    Stream.tails(Stream(1,2,3)).map(_.toList).toList shouldEqual List(List(1,2,3), List(2,3), List(3), List())
  }
  "scanRight" should "return all intermediate results of applying f to x in X" in {
    Stream.scanRight(0)(Stream(1,2,3))(_ + _).toList shouldEqual List(6,5,3,0)
  }
}
