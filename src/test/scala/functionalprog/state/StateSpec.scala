package functionalprog.state

import org.scalatest._
import functionalprog.state._

class StateSpec extends FlatSpec with Matchers {
  "The nonNegativeInt function" should "return an int between 0 and Int.MaxValue (inclusive)" in {
    RNG.nonNegativeInt(RNG.Simple(3))._1 >= 0
  }
  "The double function" should "return a double between 0 and 1 (exclusive)" in {
    val n = RNG.double(RNG.Simple(10))._1
    n >= 0 && n < 1
  }
  "simulateMachine" should "simulate a series of inputs and return State[Machine, (Int, Int)]" in {
    val candies = 10
    val coins = 0
    val locked = true
    val m = Machine(locked, candies, coins)
    val ins = List(Coin, Turn, Coin, Turn)
    val ((coins2, candies2), _)  = Candy.simulateMachine(ins).run(m)
    candies2 shouldEqual 8
    coins2 shouldEqual 2
  }
}
