package functionalprog.testing

import org.scalatest._
import functionalprog.state._

class GenSpec extends FlatSpec with Matchers {
  "Gen" should "allow us to generate a pair of (Int, Int) in some range" in {
    val xs = Gen.listOfN(2, Gen.choose(1, 50)).sample.run(RNG.Simple(3))._1
    xs.forall(x => x >= 1 && x < 50)
  }
  "Gen" should "generate strings using existing primitives" in {
    val xs = Gen.listOfN(2, Gen.choose(1, 50)).sample.map(_.toString).run(RNG.Simple(3))._1
    println(xs)
    xs
  }
}


