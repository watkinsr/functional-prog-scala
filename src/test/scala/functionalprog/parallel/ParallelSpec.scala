package functionalprog.parallel

import org.scalatest._
import functionalprog.parallel._
import java.util.concurrent._

class ParallelSpec extends FlatSpec with Matchers {
  "sum" should "summate a IndexedSeq[A] in parallel" in {
    val pool = Executors.newFixedThreadPool(8)
    val p = Examples.sum(List(1,2,3,4).toIndexedSeq)
    val res = p(pool).get
    res shouldEqual 10
  }
  "paragraphWordCount" should "return the number of words across all paragraphs" in {
    val pool = Executors.newFixedThreadPool(8)
    val p = Examples.paragraphWordCount(List("The brown fox", "Ran over the hill", "With nothing.").toIndexedSeq)
    val res = p(pool).get
    res shouldEqual 9
  }
}
