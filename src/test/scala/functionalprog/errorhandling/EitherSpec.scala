package functionalprog.errorhandling

import org.scalatest._
import scala.{Either => _}
import functionalprog.errorhandling._
import functionalprog.datastructures._

class EitherSpec extends FlatSpec with Matchers {
  "The map function" should "apply an f to a Right value" in {
     Right(2).map((a: Int) => a * a) shouldEqual Right(4)
  }
  "The map function" should "not apply an f to a Left value" in {
     Left("foo").map((a: Int) => a * a) shouldEqual Left("foo")
  }
  "The flatMap function" should "flatten given an f" in {
     Right(2).flatMap((a: Int) => Right(a * a)) shouldEqual Right(4)
     Left("foo").flatMap((a: Int) => Right(a * a)) shouldEqual Left("foo")
  }
  "The orElse function" should "Provide a Right(x) given a Right(x)" in {
     Right(2).orElse(Left("foo")) shouldEqual Right(2)
  }
  "The orElse function" should "return a b given a Left(x)" in {
     Left(-1).orElse(Left("Something else")) shouldEqual Left("Something else")
  }
  "The map2 function" should "apply an f given two Right(x)" in {
     Right(2).map2(Right(3))(_ * _) shouldEqual Right(6)
  }
  "The map2 function" should "apply return the original Left if the first Either is Left(x)" in {
     Left(-1).map2(Right(3))((a, b) => b) shouldEqual Left(-1)
  }
  "The map2 function" should "apply return the sechond Left if the second Either is Left(x)" in {
     Right(2).map2(Left("Nonsense"))((a, b) => a) shouldEqual Left("Nonsense")
  }
  "The traverse function" should "turn a List[A] into Either[E, List[A]" in {
    Either.traverse(List(1,2,3))((a) => Right(a * a)) shouldEqual Right(List(1,4,9))
  }
  "The traverse function" should "return Left at the first failure" in {
    Either.traverse(List("1", "a", "2"))((a) => Either.Try(a.toInt) orElse Left("fail")) shouldEqual Left("fail")
  }
  "The sequence function" should "return Right(List(x)) given a valid List" in {
    Either.sequence(List(Right(1), Right(2), Right(3))) shouldEqual Right(List(1,2,3))
  }
  "The sequence function" should "return Left(x) if there exists a Left within a List[Either[E,A]]" in {
    Either.sequence(List(Right(1), Left("woops"), Right(3))) shouldEqual Left("woops")
  }
  "The Person.mkPerson function" should "return two Left(x) if there are two errors" in {
    Person.mkPerson("", -1) shouldEqual Left("Name is empty. Age is out of range.")
  }
  "The Person.mkPerson function" should "return the correct Left(x) is there is one error" in {
    Person.mkPerson("", 25) shouldEqual Left("Name is empty.")
    Person.mkPerson("John", -2) shouldEqual Left("Age is out of range.")
  }
  "The Person.mkPerson function" should "make a person if the params are correct" in {
    val p = Person.mkPerson("John", 25) match {
      case Left(s) => false
      case Right(p) => true
    }
  }
}
