import org.scalatest._
import functionalprog.errorhandling._
import functionalprog.datastructures._

class OptionSpec extends FlatSpec with Matchers {
  "sequence" should "combine list of options into one option containing list of all Some or None" in {
    Option.sequence(List(Some(1), Some(2))) shouldEqual Some(List(1,2))
    Option.sequence(List(Some(1), Some(2), None, Some(4))) shouldEqual None
  }
  "sequenceTraverse" should "combine list of options into one option containing list of all Some or None" in {
    Option.sequenceTraverse(List(Some(1), Some(2))) shouldEqual Some(List(1,2))
    Option.sequenceTraverse(List(Some(1), Some(2), None, Some(4))) shouldEqual None
  }
  "parseInts" should "parse string values to Option[Int]" in {
    Option.parseInts(List("1", "2")) shouldEqual Some(List(1, 2))
    Option.parseInts(List("a", "2")) shouldEqual None
    Option.parseInts(List("a")) shouldEqual None
  }
}
